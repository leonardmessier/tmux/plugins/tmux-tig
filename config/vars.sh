#!/usr/bin/env bash

TMUX_TIG_COMMAND="tig"
TMUX_TIG_DEFAULT_KEY="g"
TMUX_TIG_LOG_KEY="l"
TMUX_TIG_ARGS="--all"
TMUX_TIG_TITLE_ICON=""
TMUX_TIG_TITLE="TIG"

eval "$(direnv export bash)" > /dev/null
