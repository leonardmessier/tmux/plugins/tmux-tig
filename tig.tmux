#!/usr/bin/env bash

CURRENT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source "${CURRENT_DIR}/config/vars.sh"

set_normal_bindings() {
    # Initializing the trigger key to prefix + g
    tmux bind-key -T prefix "${TMUX_TIG_DEFAULT_KEY}" switch-client -T tmux_git_table_1

    # Git log with tig
    tmux bind-key -T tmux_git_table_1 "${TMUX_TIG_LOG_KEY}" run-shell -b "${CURRENT_DIR}/scripts/tmux-tig.sh"
}

main() {
    set_normal_bindings
}

main
