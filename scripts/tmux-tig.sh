#!/usr/bin/env bash

set -e

CURRENT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
source "$CURRENT_DIR/../config/vars.sh"

main() {
    local CMD="$TMUX_TIG_COMMAND"

    if [ -n "$TMUX_TIG_ARGS" ]; then
        CMD+=" $TMUX_TIG_ARGS"
    fi

    tmux new-window \
        -n "${TMUX_TIG_TITLE_ICON} ${TMUX_TIG_TITLE}" \
        -a \
        -c "$(pwd)" \
        -S \
        "${CMD}"
}

main
